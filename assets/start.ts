import jszipHelper from "./jszipHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class start extends cc.Component {

    private _zipHelper: jszipHelper = null

    @property(cc.Sprite)
    sp: cc.Sprite = null

    @property(cc.VideoPlayer)
    videoPlayer: cc.VideoPlayer = null

    @property(cc.Label)
    desLabel: cc.Label = null

    @property(cc.Node)
    rootNode: cc.Node = null

    @property(cc.Node)
    loadingNode: cc.Node = null

    /** 上次点击的node */
    private _preClickNode:cc.Node =null

    protected onLoad(): void {
        this.loadingNode.active = true
        this.rootNode.active = false
        this.videoPlayer.node.active =false
        this.desLabel.node.active =false
        this.sp.node.parent.active =false
        this._zipHelper = new jszipHelper()
        this._zipHelper.init('resources/test.zip', () => {
            this.loadingNode.active = false
            this.rootNode.active = true
        })
    }

    public onVideoEvnet(player: cc.VideoPlayer, e: cc.VideoPlayer.EventType) {
        if (e == cc.VideoPlayer.EventType.READY_TO_PLAY) {
            player.play()
        }
        console.log(e)
    }

    public onClickVideo(e:cc.Event.EventTouch) {
        if(this._preClickNode==e.target)return
        this._preClickNode =e.target
        this.sp.node.parent.active = false
        this.videoPlayer.node.active = true
        this.desLabel.node.active =false
        this._zipHelper.load('test/video.mp4', cc.VideoClip, (data: any) => {
            this.videoPlayer.resourceType = cc.VideoPlayer.ResourceType.REMOTE
            if (this.videoPlayer.remoteURL == data) {
                this.videoPlayer.currentTime = 0
                this.videoPlayer.pause()
                this.scheduleOnce(() => {
                    this.videoPlayer.play()
                })
            } else {
                this.videoPlayer.remoteURL = data
            }
        })
    }

    public onClickPic(e:cc.Event.EventTouch) {
        if(this._preClickNode==e.target)return
        this._preClickNode =e.target
        this.videoPlayer.node.active = false
        this.sp.node.parent.active = true
        this.desLabel.node.active =false
        this._zipHelper.load('test/one.jpg', cc.SpriteFrame, (sp: cc.SpriteFrame) => {
            this.sp.spriteFrame = sp
        })
    }

    public onClickJson(e:cc.Event.EventTouch) {
        if(this._preClickNode==e.target)return
        this._preClickNode =e.target
        this.videoPlayer.node.active = false
        this.sp.node.parent.active = false
        this.desLabel.node.active =true
        this._zipHelper.load('test/test.json', cc.JsonAsset, (json: JSON) => {
            this.desLabel.string=JSON.stringify(json)
        })
    }
}
